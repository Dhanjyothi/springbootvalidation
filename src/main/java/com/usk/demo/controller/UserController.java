package com.usk.demo.controller;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.usk.demo.dto.UserRequestDto;


@RestController
@RequestMapping("/users")
@Validated
public class UserController {

	@PostMapping("/save")
	public ResponseEntity<String> saveUser(@Valid @RequestBody UserRequestDto userRequestDto) {
		return new ResponseEntity<String>("Success", HttpStatus.CREATED);
	}
	@GetMapping("/getUser")
	public ResponseEntity<String> getUser(@RequestParam("name") @NotEmpty @Size(min=4,max=20) String name)
	{
		return new ResponseEntity<String>("Success", HttpStatus.OK);
	}
	@GetMapping("/getUserByPath/{name}")
	public ResponseEntity<String> getUserByPath(@PathVariable("name") @NotEmpty @Size(min=4,max=20) String name)
	{
		return new ResponseEntity<String>("Success", HttpStatus.OK);
	}
	
	
}
